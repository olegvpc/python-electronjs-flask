const myform = document.getElementById('myform')
const title = document.getElementById('title')
const body = document.getElementById('body')
const description = document.getElementById('description')

const articles = document.getElementById('articles')

function addRecord(newData) {
  fetch('http://127.0.0.1:5000/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newData)
  })
    .then((res) => res.json())
    .then((data) => {
      // console.log(data)
      getAllData()
    })
    .catch((err) => console.log(`Error in fetch post-add: ${err}`))
}

const getAllData = () => {
  fetch('http://127.0.0.1:5000/get', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => res.json())
    .then((data) => {
      console.log(data)
      renderArticles(data)
    })
    .catch((err) => console.log(`Error in fetch get all: ${err}`))
}
getAllData()

function deleteRecord (id) {
    fetch(`http://127.0.0.1:5000/delete/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then((data) => getAllData())
}

function getDataById(id) {
  fetch(`http://127.0.0.1:5000/get/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .then((data) => renderOneItem(data))
}

function renderOneItem(item) {
  // console.log(item)
  title.value = item.title
  body.value = item.body
  description.value = item.description
}

function renderArticles (myData) {
  articles.innerHTML = ''
  myData.forEach((item) => {
    articles.innerHTML +=
      `
        <div class="card card-body my-2">
          <h2>${item.title}</h2>
          <p>${item.body}</p>
          <h6>${String(item.date).replace('T', " - ")}</h6>
          <p>
            <button class="btn btn-danger" onclick="deleteRecord(${item.id})">Delete</button>
            <button class="btn btn-success" onclick="getDataById(${item.id})">Update</button>
          </p>
        </div>
      `
  })
}

function handleSubmit(e) {
  e.preventDefault()
  // console.log('Hello World')
  const newData = {
    title: title.value,
    body: body.value,
    description: description.value
  }

  addRecord(newData)
  myform.reset()
}

myform.addEventListener('submit', (e) => handleSubmit(e))