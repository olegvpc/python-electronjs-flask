from flask import Flask , jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import datetime


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:admin@localhost/flask_local'
#  'mysql://root:admin@localhost/flask' - root is username and flask is database-name
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy()
db.init_app(app)
# OR db = SQLAlchemy(app)
ma = Marshmallow(app)


class Articles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    body = db.Column(db.Text())
    description = db.Column(db.Text())
    date = db.Column(db.DateTime, default=datetime.datetime.now)

    def __init__(self, title, body, description):
        self.title = title
        self.body = body
        self.description = description


class ArticleSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'body', 'description', 'date')


article_schema = ArticleSchema()
articles_schema = ArticleSchema(many=True)


# first creating table in db ( need to install <brew install mysql>) after in terminal -> Python3 -> from app import db
# https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/quickstart/
with app.app_context():
    db.create_all()


@app.route('/get', methods=['GET'])
def get_articles():
    all_articles = Articles.query.all()
    results = articles_schema.dump(all_articles)
    return jsonify(results)
    # return jsonify({'Hello': 'World'}) # test record


@app.route('/get/<id>', methods=['GET'])
def post_details(id):
    article = Articles.query.get(id)
    return article_schema.jsonify(article)


@app.route('/add', methods=['POST'])
def add_article():
    title = request.json['title']
    body = request.json['body']
    description = request.json['description']

    articles = Articles(title, body, description)
    db.session.add(articles)
    db.session.commit()
    return article_schema.jsonify(articles)


@app.route('/update/<id>', methods=['PUT'])
def update_article(id):
    article = Articles.query.get(id)

    title = request.json['title']
    body = request.json['body']
    description = request.json['description']

    article.title = title
    article.body = body
    article.description = description
    db.session.commit()
    return article_schema.jsonify(article)


@app.route('/delete/<id>', methods=['DELETE'])
def article_delete(id):
    try:
        article = Articles.query.get(id)
        db.session.delete(article)
        db.session.commit()
        # return ({'result of delete': 'success'})
        return article_schema.jsonify(article)  # return deleted object
    except Exception as e:
        return ({
            'message': 'No such id-record',
            'error': e
        })


if __name__ == '__main__':
    app.run(debug=False)
    # app.run(debug=True)